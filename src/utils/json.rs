use std::collections::{HashMap, HashSet};

use crate::components::Error;

/// JSON helper.
pub trait ColumnsSelector {
  type Error;

  /// Select individual columns in JSON value.
  fn columns(&self, cols: Vec<&str>) -> Result<Self, Self::Error>
  where
    Self: Sized;
}

impl ColumnsSelector for serde_json::Value {
  type Error = Error;

  /// Select individual columns in JSON value.
  /// Both map and array of maps is supported.
  /// NOTE: Invalid columns are simply ignored.
  ///
  /// # Errors
  /// In case invalid type is provided `Error::InvalidTypeJSON` is returned.
  fn columns(&self, cols: Vec<&str>) -> Result<Self, Self::Error> {
    let cols: HashSet<&str> = HashSet::from_iter(cols.iter().cloned());

    if let serde_json::Value::Object(map) = self {
      let filtered_map = filter_map(map, &cols);
      return Ok(serde_json::json!(filtered_map));
    }

    if let serde_json::Value::Array(array) = self {
      let mut filtered_array = vec![];
      for el in array {
        let map = el.as_object().ok_or(Error::InvalidTypeJSON)?;
        let filtered_map = filter_map(map, &cols);
        filtered_array.push(filtered_map);
      }
      return Ok(serde_json::json!(filtered_array));
    }

    Err(Error::InvalidTypeJSON)
  }
}

/// Filters JSON map by selecing only given columns.
fn filter_map<'a>(
  map: &'a serde_json::Map<String, serde_json::Value>,
  cols: &'a HashSet<&str>,
) -> HashMap<&'a std::string::String, &'a serde_json::Value> {
  let filtered_map: HashMap<&String, &serde_json::Value> = map
    .into_iter()
    .filter(|(key, _val)| cols.contains(key.as_str()))
    .collect();
  filtered_map
}
