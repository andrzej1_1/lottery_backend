use anyhow::{Context, Result};
use lottery_core::LotteryCore;

use std::sync::Arc;

use components::App;

mod bootstrap;
mod components;
mod controllers;
mod utils;

#[rocket::main]
async fn main() -> Result<()> {
  // Create main application component, that holds connection pools.
  let app = App::new().context("Unable to start application.")?;
  let app = Arc::new(app);

  // Create lottery core and initialize it using app.
  let mut lottery = LotteryCore::new();
  app
    .initalize_lottery(&mut lottery)
    .await
    .context("Unable to initialize lottery.")?;

  // Spawn task that will run lottery rounds.
  let app_ref = app.clone();
  tokio::spawn(async move {
    loop {
      if let Err(e) = app_ref.execute_lottery_round(&mut lottery).await {
        eprintln!("Error occured while running lottery round: {:?}", e);
      }
    }
  });

  // Get CORS configuration.
  let cors = bootstrap::Cors::get_default().context("Unable to configure CORS.")?;

  // Run API server.
  let server = rocket::build()
    .manage(app)
    .mount(
      "/api",
      rocket::routes![
        controllers::timesync::timesync_req,
        controllers::seed::seed_list,
        controllers::seed::seed_get,
        controllers::bet::bet,
        controllers::bet::bet_list,
        controllers::result::result_list,
        controllers::result::result_get,
        controllers::user::user_wallet_get,
      ],
    )
    .attach(cors);
  server.launch().await.context("Unable to launch server.")?;

  Ok(())
}
