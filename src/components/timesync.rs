/// Time synchronization component.
pub struct Timesync {}

impl Timesync {
  /// Generates json response for timesync request.
  ///
  /// NOTE: timesync algorithm is described here:
  /// https://web.archive.org/web/20160310125700/http://mine-control.com/zack/timesync/timesync.html.
  pub fn make_response(req_id: &u32) -> serde_json::Value {
    // Get current timestamp.
    let timestamp_ms = chrono::Utc::now().timestamp_millis();

    // Return json.
    serde_json::json!({
      "id": req_id,
      "result": timestamp_ms
    })
  }
}
