use lottery_core::error::CoreError;
use lottery_core::LotteryCore;

use crate::bootstrap;
use crate::components::Error;

/// Holder of connection pools, interface for running lottery core.
pub struct App {
  /// Redis pool.
  pub redis: bootstrap::Redis,
  /// WAMP pool.
  pub wamp: bootstrap::Wamp,
}

impl App {
  /// Constructs new App along with connection pools.
  ///
  /// # Error
  /// This function returns `CoreError` when pool cannot be created.
  pub fn new() -> Result<Self, CoreError> {
    Ok(Self {
      redis: bootstrap::Redis::new()?,
      wamp: bootstrap::Wamp::new()?,
    })
  }

  /// Initializes lottery core, mainly performs base database operations.
  /// It is required to be called before running rounds.
  ///
  /// # Error
  /// This function returns `CoreError` when connection cannot be obtained or
  /// core cannot be initialized.
  pub async fn initalize_lottery(&self, lottery: &mut LotteryCore) -> Result<(), Error> {
    // Get connections.
    let mut redis_con = self.redis.get_con().await?;
    let wamp_client = self.wamp.get_con().await?;

    // Run core initialization.
    lottery.initialize(&mut redis_con, &wamp_client).await?;

    Ok(())
  }

  /// Waits for next round time, then executes single lottery round.
  ///
  /// # Error
  /// This function returns `CoreError` when connection cannot be obtained or
  /// core cannot be initialized.
  pub async fn execute_lottery_round(&self, lottery: &mut LotteryCore) -> Result<(), Error> {
    // Wait until next round time.
    let time = lottery.wait_until_ready().await;

    // Get connections.
    let redis_con = self.redis.get_con().await?;
    let wamp_client = self.wamp.get_con().await?;

    // Run single round.
    lottery.run_round(time, &wamp_client, redis_con).await?;

    Ok(())
  }
}
