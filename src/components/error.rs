use rocket::http::Status;
use rocket::response::{self, status::Custom};
use rocket::Request;
use thiserror::Error as ThisError;

/// Application error.
#[derive(ThisError, Debug)]
pub enum Error {
  /// Indicates that wrong JSON type was provided.
  #[error("Expected different type of JSON value, e.g. array instead of number.")]
  InvalidTypeJSON,

  /// Error from lottery core.
  #[error("Core error.")]
  Core(#[from] lottery_core::error::CoreError),

  /// Error from ORM library.
  #[error("ActiveRecord error.")]
  ActiveRecord(#[from] redis_orm::error::Error),

  /// Common application error for 404 Not Found.
  #[error("Item was not found.")]
  NotFound(String),

  /// Common application error for 422 Unprocessable Entity.
  #[error("Invalid user data.")]
  UnprocessableEntity(String),
}

impl<'r, 'o: 'r> response::Responder<'r, 'o> for Error {
  /// Decides how to represent application error to end user.
  fn respond_to(self, req: &'r Request<'_>) -> response::Result<'o> {
    // Log error to console.
    eprintln!("Error occured in endpoint: {:?}", self);

    // Represent web errors directly, e.g. Not Found.
    // Other ones are represented by Internal Server Error.
    match self {
      Self::NotFound(msg) => Custom(Status::NotFound, msg).respond_to(req),
      Self::UnprocessableEntity(msg) => Custom(Status::UnprocessableEntity, msg).respond_to(req),
      _ => Status::InternalServerError.respond_to(req),
    }
  }
}
