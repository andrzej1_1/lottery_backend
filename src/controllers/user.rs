use lottery_core::models::User;
use redis_orm::active_query::Filter;
use redis_orm::active_query::RedisVal;
use redis_orm::active_record::ActiveRecord;
use rocket::serde::json;
use rocket::serde::json::serde_json::json;
use rocket::State;

use std::sync::Arc;

use crate::components::App;
use crate::components::Error;

/// Common return type for API controllers.
pub type JsonResponse = Result<json::Value, Error>;

/// Gets wallet balance of given user.
///
/// # Errors
/// In case of missing record or database issue `Error` is returned.
#[rocket::get("/user/wallet/<user_fp>")]
pub async fn user_wallet_get(user_fp: &str, app: &State<Arc<App>>) -> JsonResponse {
  // Get connection.
  let mut con = app.redis.get_con().await?;

  // Find user by fingerprint.
  let record = User::find()
    .filter(Filter::Condition(
      "fingerprint".into(),
      RedisVal::String(user_fp.into()),
    ))
    .one::<User>(&mut con)
    .await?;

  // If user does not exist, let us create it,
  // since this is DEMO application without login/register functionality.
  // TODO: Remove in future.
  let (user, _user_pk) = match record {
    Some(r) => r,
    None => User::new(user_fp).db_save(&mut con).await?,
  };

  // Respond with user wallet balance.
  Ok(json!({"status": "ok", "balance": user.wallet()}))
}
