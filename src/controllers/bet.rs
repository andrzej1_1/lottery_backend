use lottery_core::error::CoreError;
use lottery_core::helpers::WampHelper;
use lottery_core::models::LotteryBet;
use lottery_core::models::LotteryRound;
use lottery_core::models::User;
use lottery_core::models::UserId;
use redis_orm::active_query::Filter;
use redis_orm::active_query::QuerySort;
use redis_orm::active_query::RedisVal;
use redis_orm::active_record::ActiveRecord;
use redis_orm::mutex::Mutex;
use rocket::serde::json;
use rocket::serde::json::serde_json::json;
use rocket::State;
use serde::Deserialize;
use serde::Serialize;

use std::sync::Arc;

use crate::components::App;
use crate::components::Error;
use crate::utils::json::ColumnsSelector;

/// Common return type for API controllers.
pub type JsonResponse = Result<json::Value, Error>;

/// Bet action body structure.
#[derive(Debug, Default, Serialize, Deserialize)]
pub struct BetData {
  /// User fingerprint.
  user_fp: String,
  /// Selected numbers.
  numbers: Vec<u8>,
}

/// Price of placing bet.
const BET_PRICE: u32 = 100;

/// Places new bet in lottery.
/// User wallet is updated and WAMP router notified.
///
/// # Errors
/// In case of database/wamp issue `Error` is returned.
#[rocket::post("/bet", data = "<data>")]
pub async fn bet(data: rocket::serde::json::Json<BetData>, app: &State<Arc<App>>) -> JsonResponse {
  // Get connection.
  let mut con = app.redis.get_con().await?;

  // Load incoming data.
  let bet_data: BetData = data.into_inner();

  // Get user data, check balance and then update wallet.
  // NOTE: Operation is protected with mutex named "User".
  let mutex_factory = Mutex::new();
  let guard = Mutex::new().lock("User".into(), &mut con).await?;
  let (mut user, _user_pk) = User::find()
    .filter(Filter::Condition(
      "fingerprint".into(),
      RedisVal::String(bet_data.user_fp),
    ))
    .one::<User>(&mut con)
    .await?
    .ok_or_else(|| Error::NotFound("Unable to find user.".into()))?;
  if user.wallet() < BET_PRICE as u64 {
    return Ok(json!({"status": "err", "msg": "Not enough funds."}));
  }
  user.remove_from_wallet(BET_PRICE as u32);
  let (user, user_pk) = user.db_save(&mut con).await?;
  mutex_factory.unlock(&guard, &mut con).await?;

  // Send notification about new wallet state.
  let user_fp = user.fingerprint();
  let wamp_client = app.wamp.get_con().await?;
  let topic = format!("com.lottery.user_{user_fp}.wallet");
  let kwargs = json!({
    "balance": user.wallet(),
    "action": "bet",
  });
  wamp_client.pub_kw(topic.as_str(), kwargs).await?;

  // Save new bet.
  let (_round, round_id) = LotteryRound::find()
    .order_by(QuerySort::Desc("id".into()))
    .limit(1)
    .one::<LotteryRound>(&mut con)
    .await?
    .ok_or_else(|| CoreError::MissingData("Unable to get curren round.".into()))?;
  let bet = LotteryBet::new(round_id, user_pk as UserId, bet_data.numbers)
    .map_err(|_| Error::UnprocessableEntity("Invalid data.".into()))?;
  bet.db_save(&mut con).await?;

  Ok(json!({"status": "ok"}))
}

/// Lists all unaccounted bets for given user.
/// User wallet is updated and WAMP router notified.
///
/// # Errors
/// In case of database issue `Error` is returned.
#[rocket::get("/bet/list/<user_fp>")]
pub async fn bet_list(user_fp: &str, app: &State<Arc<App>>) -> JsonResponse {
  // Get connection.
  let mut con = app.redis.get_con().await?;

  // Resolve fingeprint to user ID.
  let user_record = User::find()
    .filter(Filter::Condition(
      "fingerprint".into(),
      RedisVal::String(user_fp.into()),
    ))
    .one::<User>(&mut con)
    .await?;

  // If user does not exist, let us create it,
  // since this is DEMO application without login/register functionality.
  // TODO: Remove in future.
  let (_user, user_pk) = match user_record {
    Some(r) => r,
    None => User::new(user_fp).db_save(&mut con).await?,
  };

  // Find unaccounted bets by user ID.
  let bets = LotteryBet::find()
    .filter(Filter::AndCondition(vec![
      Filter::Condition("is_accounted".into(), RedisVal::Bool(false)),
      Filter::Condition("user_id".into(), RedisVal::Numeric(user_pk)),
    ]))
    .order_by(QuerySort::Desc("id".into()))
    .all::<LotteryBet>(&mut con)
    .await?;
  let (bets, _bets_pk): (Vec<LotteryBet>, Vec<i64>) = bets.into_iter().unzip();

  // Choose columns that will be included in response.
  let bets = json!(bets).columns(vec!["id", "numbers", "user_id", "round_id", "is_accounted"])?;

  Ok(json!({ "status": "ok", "items": bets }))
}
