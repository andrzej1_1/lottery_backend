use rocket::serde::json;
use rocket::serde::Deserialize;

use crate::components::Error;
use crate::components::Timesync;

/// Common return type for API controllers.
pub type JsonResponse = Result<json::Value, Error>;

/// Timesync request body structure.
#[derive(Deserialize)]
pub struct TimesyncRequest {
  pub id: u32,
}

/// Responds to timesync request in order to allow user synchronize time
/// with server. It is something à la NTP.
#[rocket::post("/timesync", data = "<request>")]
pub async fn timesync_req(request: json::Json<TimesyncRequest>) -> JsonResponse {
  Ok(Timesync::make_response(&request.id))
}
