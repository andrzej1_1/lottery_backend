/// Bet controller.
pub mod bet;

/// Result controller.
pub mod result;

/// Seed controller.
pub mod seed;

/// Timesync controller.
pub mod timesync;

/// User controller.
pub mod user;
