use lottery_core::models::RoundResult;
use redis_orm::active_query::Filter;
use redis_orm::active_query::QuerySort;
use redis_orm::active_query::RedisVal;
use redis_orm::active_record::ActiveRecord;
use rocket::serde::json;
use rocket::serde::json::serde_json::json;
use rocket::State;

use std::sync::Arc;

use crate::components::App;
use crate::components::Error;
use crate::utils::json::ColumnsSelector;

/// Common return type for API controllers.
pub type JsonResponse = Result<json::Value, Error>;

/// Size of page with results.
const PAGE_SIZE_RESULTS: u32 = 10;

/// Helper for calculating ceil of integer division.
pub const fn div_ceil(lhs: u32, rhs: u32) -> u32 {
  let d = lhs / rhs;
  let r = lhs % rhs;
  if r > 0 && rhs > 0 {
    d + 1
  } else {
    d
  }
}

/// Lists lottery results.
/// Selected page is returned, if ommited the first page by default.
///
/// # Errors
/// In case of database issue `Error` is returned.
#[rocket::get("/result?<page>")]
pub async fn result_list(page: Option<u32>, app: &State<Arc<App>>) -> JsonResponse {
  // Get connection.
  let mut con = app.redis.get_con().await?;

  // Determine page number.
  let page = page.unwrap_or(1);

  // Fetch total number of pages.
  let total_records = RoundResult::db_count_all(&mut con).await?;
  let total_pages = div_ceil(total_records, PAGE_SIZE_RESULTS);

  // Fetch single page.
  let from = ((page - 1) * PAGE_SIZE_RESULTS) as isize;
  let records = RoundResult::find()
    .offset(from as u32)
    .limit(PAGE_SIZE_RESULTS)
    .order_by(QuerySort::Desc("timestamp".into()))
    .all::<RoundResult>(&mut con)
    .await?;
  let (records, _records_pk): (Vec<RoundResult>, Vec<i64>) = records.into_iter().unzip();

  // Choose columns that will be included in response.
  let records = json!(records).columns(vec!["id", "numbers", "timestamp"])?;

  Ok(json!({ "status": "ok", "items": records, "current_page": page, "total_pages": total_pages }))
}

/// Gets lottery result with given ID.
///
/// # Errors
/// In case of missing record or database issue `Error` is returned.
#[rocket::get("/result/<id>")]
pub async fn result_get(id: u16, app: &State<Arc<App>>) -> JsonResponse {
  // Get connection.
  let mut con = app.redis.get_con().await?;

  // Find result by ID.
  let record = RoundResult::find()
    .filter(Filter::Condition("id".into(), RedisVal::Numeric(id.into())))
    .one::<RoundResult>(&mut con)
    .await?;

  // Send approprate response: selected columns or NotFound error.
  match record {
    Some((result, _result_pk)) => {
      let record = json!(result).columns(vec!["id", "numbers", "timestamp"])?;
      Ok(record)
    }
    None => Err(Error::NotFound("Result not found.".to_string())),
  }
}
