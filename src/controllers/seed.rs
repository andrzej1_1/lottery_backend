use lottery_core::models::LotteryRound;
use lottery_core::models::Seed;
use redis_orm::active_query::Filter;
use redis_orm::active_query::QuerySort;
use redis_orm::active_query::RedisVal;
use redis_orm::active_record::ActiveRecord;
use rocket::serde::json;
use rocket::serde::json::serde_json::json;
use rocket::State;

use std::sync::Arc;

use crate::components::App;
use crate::components::Error;
use crate::utils::json::ColumnsSelector;

/// Common return type for API controllers.
pub type JsonResponse = Result<json::Value, Error>;

/// Lists all unaccounted bets for given user.
/// User wallet is updated and WAMP router notified.
///
/// # Errors
/// In case of database issue `Error` is returned.
#[rocket::get("/seed")]
pub async fn seed_list(app: &State<Arc<App>>) -> JsonResponse {
  // Get connection.
  let mut con = app.redis.get_con().await?;

  // Get all seeds, ordered by ID desc.
  // Hide key if seed should not be visible to end user.
  let seeds = Seed::find()
    .order_by(QuerySort::Desc("id".into()))
    .all::<Seed>(&mut con)
    .await?;
  let (mut seeds, seeds_pk): (Vec<Seed>, Vec<i64>) = seeds.into_iter().unzip();
  seeds.iter_mut().for_each(|seed| {
    if !seed.is_visible() {
      seed.hide_key()
    }
  });

  // Obtain first round IDs.
  // NOTE: It is done manually, until orm supports relations.
  let mut first_rounds: Vec<u32> = vec![];
  for seed_pk in &seeds_pk {
    let first_round_id = LotteryRound::find()
      .filter(Filter::Condition(
        "seed_id".into(),
        RedisVal::Numeric(*seed_pk),
      ))
      .min("id".to_string(), &mut con)
      .await?;
    first_rounds.push(first_round_id as u32)
  }

  // Choose columns that will be included in response.
  let seeds = json!(seeds).columns(vec!["id", "is_visible", "key", "hash"])?;

  Ok(json!({ "status": "ok", "items": seeds, "first_rounds": first_rounds }))
}

/// Gets seed with given ID.
///
/// # Errors
/// In case of missing record or database issue `Error` is returned.
#[rocket::get("/seed/<id>")]
pub async fn seed_get(id: u16, app: &State<Arc<App>>) -> JsonResponse {
  // Get connection.
  let mut con = app.redis.get_con().await?;

  // Find seed by ID.
  let seed = Seed::find()
    .filter(Filter::Condition("id".into(), RedisVal::Numeric(id.into())))
    .one::<Seed>(&mut con)
    .await?;

  // Send approprate response: selected columns or NotFound error.
  match seed {
    Some((mut seed, _seed_pk)) => {
      seed.hide_key();
      let seed = json!(seed).columns(vec!["id", "is_visible", "key", "hash"])?;
      Ok(json!(seed))
    }
    None => Err(Error::NotFound("Seed not found.".to_string())),
  }
}
