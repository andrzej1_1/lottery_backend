use rocket_cors::AllowedOrigins;

/// Factory for CORS settings used in Rocket server.
pub struct Cors {}

impl Cors {
  /// Gets default CORS settings, where all origins are allowed.
  /// WARNING: This is not secure for production environment.
  ///
  /// # Errors
  /// This function return error in case CORS object cannot be created.
  pub fn get_default() -> Result<rocket_cors::Cors, rocket_cors::Error> {
    rocket_cors::CorsOptions {
      allowed_origins: AllowedOrigins::all(),
      ..Default::default()
    }
    .to_cors()
  }
}
