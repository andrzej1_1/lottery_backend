use deadpool_wamp::{Client, Config, Runtime};
use lottery_core::error::CoreError;

use std::env;

/// WAMP pool wrapper.
pub struct Wamp {
  pool: deadpool_wamp::Pool,
}

impl Wamp {
  /// Creates WAMP pool wrapper.
  ///
  /// # Errors
  /// `CoreError` is returned in case pool cannot be created.
  pub fn new() -> Result<Self, CoreError> {
    // Get configuration.
    let url = env::var("WAMP_URL").ok();
    let realm = env::var("WAMP_REALM").ok();
    let cfg = Config {
      url,
      realm,
      pool: None,
    };

    // Create pool.
    let pool = cfg.create_pool(Runtime::Tokio1)?;
    Ok(Self { pool })
  }

  /// Gets connection from pool.
  ///
  /// # Errors
  /// `CoreError` is returned in case connection cannot be obtained.
  pub async fn get_con(&self) -> Result<Client, CoreError> {
    let con: Client = self.pool.get().await?;
    Ok(con)
  }
}
