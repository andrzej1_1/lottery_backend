use deadpool_redis::{Config, Connection, Runtime};
use lottery_core::error::CoreError;

use std::env;

/// Redis pool wrapper.
pub struct Redis {
  pool: deadpool_redis::Pool,
}

impl Redis {
  /// Creates Redis pool wrapper.
  ///
  /// # Errors
  /// `CoreError` is returned in case pool cannot be created.
  pub fn new() -> Result<Self, CoreError> {
    // Get configuration.
    let redis_url = match env::var("REDIS_URL") {
      Ok(s) if !s.is_empty() => s,
      _ => String::from("redis://127.0.0.1:6379"),
    };
    let cfg = Config::from_url(redis_url);

    // Create pool.
    let pool = cfg.create_pool(Some(Runtime::Tokio1))?;
    Ok(Self { pool })
  }

  /// Gets connection from pool.
  ///
  /// # Errors
  /// `CoreError` is returned in case connection cannot be obtained.
  pub async fn get_con(&self) -> Result<Connection, CoreError> {
    let con = self.pool.get().await?;
    Ok(con)
  }
}
